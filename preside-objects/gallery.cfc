/**
 * @labelfield                  title
 * @dataManagerEnabled          true
 * @dataManagerGridFields       title,datecreated,datemodified
 * @dataManagerSortable         true
 * @dataManagerSortField        sort_order
 * @dataManagerDefaultSortOrder sort_order desc
 */

component {
	property name="title"        type="string"  dbtype="varchar" maxlength=100 required=true;
	property name="slug"         type="string"  dbtype="varchar" maxlength=120 uniqueIndexes="gallerySlug";
	property name="description"  type="string"  dbtype="text";
	property name="sort_order"   type="numeric" dbtype="int" generate="insert" generator="nextint" required=true adminRenderer="none";

	property name="asset_folder" relationship="many-to-one" relatedTo="asset_folder" required=true adminRenderer="galleryAssetFolder";
	property name="poster_image" relationship="many-to-one" relatedTo="asset"        required=false allowedTypes="image" adminRenderer="galleryPosterImage";
}