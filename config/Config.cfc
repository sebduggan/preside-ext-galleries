component {

	public void function configure( required struct config ) {
		var settings            = arguments.config.settings            ?: {};
		var coldbox             = arguments.config.coldbox             ?: {};
		var i18n                = arguments.config.i18n                ?: {};
		var interceptors        = arguments.config.interceptors        ?: {};
		var interceptorSettings = arguments.config.interceptorSettings ?: {};
		var cacheBox            = arguments.config.cacheBox            ?: {};
		var wirebox             = arguments.config.wirebox             ?: {};
		var logbox              = arguments.config.logbox              ?: {};
		var environments        = arguments.config.environments        ?: {};

		settings.adminSideBarItems.append( "galleries" );

		interceptors.prepend( { class="app.extensions.preside-ext-galleries.interceptors.GalleriesInterceptor", properties={} } );

		settings.assetmanager.derivatives.galleryAdminThumbnail = {
			  permissions     = "inherit"
			, transformations = [ { method="resize", args={ width=180, height=120, maintainAspectRatio=true, useCropHint=true } } ]
		};
		settings.assetmanager.derivatives.galleryFull = {
			  permissions     = "inherit"
			, transformations = [ { method="shrinkToFit", args={ width=1200, height=900 } } ]
		};
		settings.assetmanager.derivatives.galleryThumbnail = {
			  permissions     = "inherit"
			, transformations = [ { method="resize", args={ width=300, height=200, maintainAspectRatio=true, useCropHint=true } } ]
		};
	}
}