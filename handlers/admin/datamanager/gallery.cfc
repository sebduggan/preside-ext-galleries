component extends="preside.system.base.AdminHandler" {

	property name="presideObjectService" inject="presideObjectService";
	property name="messageBox"           inject="coldbox:plugin:messageBox";


	private void function extraTopRightButtonsForViewRecord( event, rc, prc, args={} ) {
		var gallerySlug = prc.record.slug ?: "";
		args.actions    = args.actions ?: [];

		args.actions.prepend( {
			  link      = event.buildLink( gallerySlug=gallerySlug )
			, btnClass  = "btn-default"
			, iconClass = "fa-external-link"
			, title     = translateResource( "cms:sitetree.preview.page.btn" )
			, target    = "_blank"
		} );
	}

	private string function postRenderRecord( event, rc, prc, args={} ) {
		galleryAssets = presideObjectService.selectData(
			  objectName = "asset"
			, filter     = { asset_folder=prc.record.asset_folder }
			, orderBy    = "gallery_sort_order"
		);

		return renderView( view="/admin/datamanager/gallery/_photos", args={ galleryAssets=galleryAssets } );
	}

	public void function saveAssetOrderAction( event, rc, prc ) {
		var galleryId          = rc.galleryId ?: "";
		var galleryAssetsOrder = listToArray( rc.galleryAssetsOrder ?: "" );
		var dao                = presideObjectService.getObject( "asset" );
		var assetId            = "";

		for( var i=1; i<=galleryAssetsOrder.len(); i++ ) {
			assetId = galleryAssetsOrder[ i ];
			dao.updateData( id=assetId, data={ gallery_sort_order=i } );
		}

		messageBox.info( translateResource( uri="preside-objects.gallery:order.update.success" ) );

		setNextEvent( url=event.buildAdminLink( objectName="gallery", recordId=galleryId ) );
	}

	public void function setPosterImage( event, rc, prc ) {
		var galleryId = rc.galleryId ?: "";
		var assetId   = rc.assetId   ?: "";

		if ( len( galleryId ) && len( assetId ) ) {
			var updated = presideObjectService.updateData( objectName="gallery", id=galleryId, data={ poster_image=assetId } );
			if ( updated ) {
				event.renderData( type="json", data={ success=true, message=translateResource( "preside-objects.gallery:poster.image.set.success" ) } );
				return;
			}
		}

		event.renderData( type="json", data={ success=false }, statusCode=400, statusText=translateResource( "preside-objects.gallery:poster.image.set.fail" ) );
	}

	public void function editCaption( event, rc, prc ) {
		prc.assetId = rc.asset ?: "";

		var asset = presideObjectService.selectData( objectName="asset", id=prc.assetId );

		for( var r in asset ) {
			prc.savedData = r;
			break;
		}

		event.setLayout( "adminModalDialog" );
	}

	public void function editCaptionAction( event, rc, prc ) {
		var assetId = rc.assetId ?: "";
		var caption = rc.description ?: "";

		presideObjectService.updateData( objectName="asset", id=assetId, data={ description=caption } );

		event.renderData( type="json", data={ success=true, message=translateResource( "preside-objects.gallery:caption.set.success" ) } );
	}

}