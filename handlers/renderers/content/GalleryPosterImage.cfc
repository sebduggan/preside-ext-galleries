component {

	private string function admin( event, rc, prc, args={} ){
		var assetId = args.data ?: "";
		var img     = "";

		if ( len( assetId ) ) {
			img = renderAsset( assetId=assetId, args={ derivative="galleryAdminThumbnail" } );
		}

		return '<div class="gallery-admin-poster-image">#img#</div>';
	}

}