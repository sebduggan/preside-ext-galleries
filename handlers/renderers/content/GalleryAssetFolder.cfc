component {

	private string function admin( event, rc, prc, args={} ){
		var folderId = args.data ?: "";
		var label    = renderLabel( "asset_folder", folderId );
		var link     = event.buildAdminLink( linkTo="assetManager", queryString="folder=#folderId#" );

		return '<a href="#link#">#label#</a>';
	}

}