component {

	property name="presideObjectService" inject="presideObjectService";
	property name="sitetreeService"      inject="sitetreeService";

	private function index( event, rc, prc, args={} ) {
		return renderView(
			  view          = "page-types/galleries_index/index"
			, presideObject = "galleries_index"
			, id            = event.getCurrentPageId()
			, args          = args
		);
	}

	public string function _gallery( event, rc, prc, args={} ) {
		var gallerySlug = prc.gallerySlug ?: "";
		args.gallery    = presideObjectService.selectData( objectName="gallery", filter={ slug=gallerySlug } );


		if ( !args.gallery.recordCount ) {
			event.notFound();
		}

		var galleriesIndexPage = siteTreeService.getPage( systemPage="galleries_index" );

		event.initializeDummyPresideSiteTreePage(
			  id               = args.gallery.id
			, title            = translateResource( uri="preside-objects.gallery:gallery.page.title", data=[ args.gallery.title ] )
			, navigation_title = "Gallery"
			, slug             = args.gallery.slug
			, parentPage       = galleriesIndexPage
		);

		if ( event.isAdminUser() ) {
			event.setEditPageLink( event.buildAdminLink( objectname="gallery", recordId=args.gallery.id ) );
		}
		rc.body = renderView(
			  view = "page-types/galleries_index/_gallery"
			, args = args
		);
		event.setView( view="/core/simpleBodyRenderer" );
	}
}
