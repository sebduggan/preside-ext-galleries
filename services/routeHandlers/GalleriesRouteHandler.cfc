component singleton=true {

// CONSTRUCTOR
	/**
	 * @presideObjectService.inject    PresideObjectService
	 */
	public any function init( required any presideObjectService ) {
		_setPresideObjectService( arguments.presideObjectService );

		return this;
	}

// ROUTE HANDLER METHODS
	public boolean function match( required string path, required any event ) {
		var galleriesRoot = _getGalleriesRoot();

		return ReFindNoCase( "^#galleriesRoot#(([^/.]+)\.html)?", arguments.path );
	}

	public void function translate( required string path, required any event ) {
		var rc            = event.getCollection();
		var prc           = event.getCollection( private=true );
		var galleriesRoot = _getGalleriesRoot();

		if ( arguments.path == galleriesRoot ) {
			event.getController().setNextEvent( url=event.buildLink( page="galleries_index" ) );
		}
		prc.gallerySlug = ReReplace( arguments.path, "^#galleriesRoot#([^/.]+)\.html$", "\1" );

		rc.event = "page-types.galleries_index._gallery";
	}

	public boolean function reverseMatch( required struct buildArgs, required any event ) {
		return Len( Trim( buildArgs.galleryId ?: "" ) ) || Len( Trim( buildArgs.gallerySlug ?: "" ) );
	}

	public string function build( required struct buildArgs, required any event ) {
		var galleriesRoot = _getGalleriesRoot();
		var gallerySlug   = trim( buildArgs.gallerySlug ?: "" );

		if ( !len( gallerySlug ) ) {
			gallerySlug = _getPresideObjectService().selectData( objectName="gallery", id=buildArgs.galleryId, selectFields=[ "slug" ] ).slug;
		}

		return event.getSiteUrl() & "#galleriesRoot##gallerySlug#.html";
	}


	private string function _getGalleriesRootFromPage() {
		_galleriesRoot = _getPresideObjectService().selectData(
			  objectName   = "page"
			, filter       = { page_type="galleries_index" }
			, selectFields = [ "_hierarchy_slug" ]
		)._hierarchy_slug;
		return _galleriesRoot;
	}

// private getters and setters
	private string function _getGalleriesRoot() {
		return _galleriesRoot ?: _getGalleriesRootFromPage();
	}

	private any function _getPresideObjectService() {
		return _presideObjectService;
	}
	private void function _setPresideObjectService( required any presideObjectService ) {
		_presideObjectService = arguments.presideObjectService;
	}

}
