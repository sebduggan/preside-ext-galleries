<cfscript>
	assetId   = prc.assetId   ?: "";
	savedData = prc.savedData ?: {};
</cfscript>

<cfoutput>
	<form method="post" id="editCaptionForm" action="#event.buildAdminLink( linkTo="datamanager.gallery.editCaptionAction" )#" class="form-horizontal edit-caption-form">
		<input type="hidden" name="assetId" value="#assetId#">

		#renderForm(
			  formName         = "preside-objects.gallery.admin.editCaption"
			, context          = "admin"
			, formId           = "result-edit"
			, validationResult = rc.validationResult ?: ""
			, savedData        = rc.formData ?: savedData
		)#
	</form>

	<div class="gallery-edit-caption-image">
		#renderAsset( assetId=assetId, args={ derivative="galleryFull" } )#
	</div>
</cfoutput>