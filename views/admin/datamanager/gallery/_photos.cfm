<cfscript>
	galleryAssets      = args.galleryAssets ?: queryNew("");
	baseEditAssetUrl   = event.buildAdminLink( linkTo="assetManager.editAsset", queryString="asset={assetId}" );
	baseEditCaptionUrl = event.buildAdminLink( linkTo="datamanager.gallery.editCaption", queryString="asset={assetId}" );
	setPosterLabel     = translateResource( "preside-objects.gallery:set.poster.label" );
	setPosterLabel     = translateResource( "preside-objects.gallery:set.poster.label" );
	editCaptionLabel   = translateResource( "preside-objects.gallery:edit.caption.label" );
	editAssetLabel     = translateResource( "preside-objects.gallery:edit.asset.label" );

	event.include( "/js/admin/specific/editGallery/" )
		.include( "/css/admin/specific/gallery/" );
</cfscript>

<cfoutput>
	<div class="widget-box">
		<div class="widget-header">
			<h4 class="widget-title lighter smaller">
				<i class="fa fad #translateResource( "preside-objects.gallery:iconclass" )#"></i>
				#translateResource( "preside-objects.gallery:assets.title" )#
			</h4>
		</div>

		<div class="widget-body">
			<div class="widget-main padding-20">
				<div id="galleryAssets" data-gallery-id="#prc.recordId#" data-set-poster-url="#event.buildAdminLink( linkTo="datamanager.gallery.setPosterImage" )#" data-admin-notifications-position="#getSetting( "adminNotificationsPosition" )#">
					<cfloop query="galleryAssets">
						<cfscript>
							editAssetUrl   = replace( baseEditAssetUrl  , "{assetId}", galleryAssets.id );
							editCaptionUrl = replace( baseEditCaptionUrl, "{assetId}", galleryAssets.id );
						</cfscript>
						<div class="gallery-asset" data-asset-id="#galleryAssets.id#">
							<div class="gallery-asset-image">
								#renderAsset( assetId=galleryAssets.id, args={ derivative="galleryAdminThumbnail" } )#
							</div>

							<div class="gallery-asset-actions">
								<a href="#editCaptionUrl#" title="#editCaptionLabel#" data-action="editCaption"><i class="fa fa-font"></i></a>
								<a href="##" title="#setPosterLabel#" data-action="setPoster"><i class="fa fa-thumbtack"></i></a>
								<a href="#editAssetUrl#" title="#editAssetLabel#" data-action="editAsset"><i class="fa fa-edit"></i></a>
							</div>
						</div>
					</cfloop>
				</div>
			</div>
		</div>

		<form action="#event.buildAdminLink( linkTo="datamanager.gallery.saveAssetOrderAction" )#" method="post" id="galleryAssetsOrderForm">
			<input type="hidden" name="galleryId"          value="#prc.recordId#">
			<input type="hidden" name="galleryAssetsOrder" value="#valueList(galleryAssets.id)#">

			<div class="form-actions">
				<div>
					<a id="resetGalleryOrderButton" class="btn btn-sm btn-default">
						<i class="fa fa-reply bigger-110"></i>
						#translateResource( "preside-objects.gallery:order.reset.label" )#
					</a>
					<button id="saveGalleryOrderButton" class="btn btn-sm btn-success" type="submit" disabled>
						<i class="fa fa-check bigger-110"></i>
						#translateResource( "preside-objects.gallery:order.save.label" )#
					</button>
				</div>
			</div>
		</form>
	</div>
</cfoutput>