<cfscript>
	gallery = args.gallery ?: {};

	event.include( "/css/frontend/mediabox/" ).include( "/js/frontend/mediabox/" );
</cfscript>

<cfoutput>
	<h1>#gallery.title#</h1>

	<cfif len( trim( gallery.description ) )>
		#renderContent( "richeditor", gallery.description )#
	</cfif>

	<div class="gallery-container">
		#renderView(
			  presideObject = "asset"
			, view          = "page-types/galleries_index/_gallery-item"
			, orderBy       = "gallery_sort_order"
			, filter        = { asset_folder=gallery.asset_folder }
			, args          = { galleryId=gallery.id }
		)#
	</div>
</cfoutput>