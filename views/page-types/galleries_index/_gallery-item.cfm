<cf_presideparam name="args.id"           />
<cf_presideparam name="args.asset_folder" />
<cf_presideparam name="args.title"        />
<cf_presideparam name="args.description"  />

<cfscript>
	galleryId = args.galleryId ?: args.asset_folder;
	fullsize  = event.buildLink( assetId=args.id, derivative="galleryFull" );
	thumbnail = event.buildLink( assetId=args.id, derivative="galleryThumbnail" );
</cfscript>

<cfoutput>
	<a href="#fullsize#"
		data-mediabox="gallery-#galleryId#"
		data-title="#htmlEditFormat( args.description )#"
		rel="noopener noreferrer"
		target="_blank"><img src="#thumbnail#" alt="#htmlEditFormat( args.description )#"></a>
</cfoutput>