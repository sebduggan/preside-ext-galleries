<cf_presideparam name="args.title"         field="page.title"        editable="true" />
<cf_presideparam name="args.main_content"  field="page.main_content" editable="true" />

<cfoutput>
	<h1>#args.title#</h1>
	#args.main_content#

	<ul class="gallery-listing">
		#renderView(
			  presideObject = "gallery"
			, view          = "page-types/galleries_index/_index-item"
			, orderBy       = "sort_order desc"
		)#
	</ul>
</cfoutput>