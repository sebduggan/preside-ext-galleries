( function( $ ) {

	$( document ).ready( function() {
		var $galleryAssets           = $( "#galleryAssets" )
		  , $saveGalleryOrderButton  = $( "#saveGalleryOrderButton" )
		  , $resetGalleryOrderButton = $( "#resetGalleryOrderButton" )
		  , originalOrder            = $galleryAssets.html()
		  , setOrder, editCaptionModal;

		$.gritter.options.position = $galleryAssets.data( "adminNotificationsPosition" );

		setOrder = function( disabled ){
			var newOrder = $galleryAssets.sortable( "toArray", { attribute: "data-asset-id" } );
			$( "[name=galleryAssetsOrder]" ).val( newOrder ).trigger( "change" );
			$saveGalleryOrderButton.prop( "disabled", disabled );
		};

		$galleryAssets.sortable( {
			  cursor    : "grabbing"
			, tolerance : "pointer"
			, update    : function( event, ui ) {
				setOrder( false );
			  }
		} );

		$galleryAssets.on( "click", ".gallery-asset-actions a", function( e ){
			var $target   = $( this )
			  , action    = $target.data( "action" )
			  , $asset    = $target.closest( ".gallery-asset" )
			  , assetId   = $asset.data( "assetId" )
			  , galleryId = $galleryAssets.data( "galleryId" )
			  , galleryModal, galleryIframe, modalOptions, callbacks, submitForm;

			e.preventDefault();

			modalOptions    = {
				title      : i18n.translateResource( "cms:assetmanager.edit.asset.title" ),
				className  : "full-screen-dialog",
				buttons : {
					cancel : {
						  label     : '<i class="fa fa-reply"></i> ' + i18n.translateResource( "cms:cancel.btn" )
						, className : "btn-default"
					},
					ok : {
						  label     : '<i class="fa fa-check"></i> ' + i18n.translateResource( "cms:save.btn" )
						, className : "btn-primary"
						, callback  : function(){ return submitForm(); }
					}
				}
			}

			callbacks = {
				onLoad : function( iframe ) {
					galleryIframe = iframe;
				}
			};

			submitForm = function() {
				var $form = $( galleryIframe.editCaptionForm );

				$.ajax( {
					  type : "POST"
					, url  : $form.attr( "action" )
					, data : $form.serializeObject()
				} ).done( function( data ) {
					galleryModal.close();
					$.gritter.add( {
						  title      : i18n.translateResource( "cms:info.notification.title" )
						, text       : data.message
						, class_name : "gritter-success"
						, sticky     : false
					} );
				} ).fail( function( jqXHR, textStatus, error ) {

				} );

				return false;
			};

			if ( action=="setPoster" ) {
				$.ajax( {
					  url    : $galleryAssets.data( "setPosterUrl" )
					, method : "GET"
					, data   : { galleryId:galleryId, assetId:assetId }
				} ).done( function( data ){
					$( ".gallery-admin-poster-image" ).html( $asset.find( ".gallery-asset-image img" ).clone() );

					$.gritter.add( {
						  title      : i18n.translateResource( "cms:info.notification.title" )
						, text       : data.message
						, class_name : "gritter-success"
						, sticky     : false
					} );
				} ).fail( function( jqXHR, textStatus, error ){
					$.gritter.add( {
						  title      : i18n.translateResource( "cms:error.notification.title" )
						, text       : error
						, class_name : "gritter-error"
						, sticky     : false
					} );
				} );
			}

			if ( action=="editCaption" ) {
				galleryModal = new PresideIframeModal( $target.attr( "href" ), "100%", "100%", callbacks, modalOptions );
				galleryModal.open();
			}

			if ( action=="editAsset" ) {
				window.location.href = $target.attr( "href" );
			}
		} );

		$resetGalleryOrderButton.on( "click", function( e ){
			e.preventDefault();
			$galleryAssets.html( originalOrder );
			setOrder( true );
		} );

	});

} )( presideJQuery );