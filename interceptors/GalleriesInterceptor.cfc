component extends="coldbox.system.Interceptor" {

	property name="presideObjectService" inject="delayedInjector:presideObjectService";

// PUBLIC
	public void function configure() {}

	public void function postPrepareXmlSitemapPages( event, interceptData ) {
		var pages      = interceptData.pages  ?: [];
		var siteId     = interceptData.siteId ?: "";
		var logger     = interceptData.logger ?: "";
		var haveLogger = !isSimpleValue( logger );
		var canInfo    = haveLogger && logger.canInfo();
		var canError   = haveLogger && logger.canError();
		var galleries  = presideObjectService.selectData( objectName="gallery" );

		if ( canInfo ) {
			logger.info( "Adding [#galleries.recordcount#] galleries..." );
		}

		for( var gallery in galleries ) {
			pages.append( {
				  loc     = event.buildLink( siteId=siteId, gallerySlug=gallery.slug )
				, lastmod = gallery.datemodified
			} );
		}
	}

}
