# Galleries for Preside

This extension allows you to configure photo galleries in your Preside application, and display them in the front end.
